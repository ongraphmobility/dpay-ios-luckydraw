//
//  DataService.swift
//  multipeerDemo2
//
//  Created by Ongraph on 15/11/17.
//  Copyright © 2017 Ongraph Technologies. All rights reserved.
//

import Foundation
import MultipeerConnectivity

protocol recieveDataDelegate:class {
    func didRecieveData(dataStr:String)
    func didChangeConnectionState(state:String, deviceName:String?)
    
}

class DataServiceManager : NSObject {
    
    private let dataServiceType = "example-data"
    private let myPeerId = MCPeerID(displayName: UIDevice.current.name)
//    private var serviceAdvertiser : MCNearbyServiceAdvertiser
    private var serviceBrowser : MCNearbyServiceBrowser
    
    weak var delegate : recieveDataDelegate?
    
    lazy var session : MCSession = {
        
        let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .required)
        session.delegate = self
        return session
    }()
    
    override init() {
        
//        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: nil, serviceType: dataServiceType)
        
        delegate?.didChangeConnectionState(state: kSearching, deviceName: nil)
        
        self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: dataServiceType)
        
        super.init()
//        
//        self.serviceAdvertiser.delegate = self
//        self.serviceAdvertiser.startAdvertisingPeer()
//        
        self.serviceBrowser.delegate = self
        self.serviceBrowser.startBrowsingForPeers()
    }
    
    deinit {
//        self.serviceAdvertiser.stopAdvertisingPeer()
        self.serviceBrowser.stopBrowsingForPeers()
    }
}

extension DataServiceManager : MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case MCSessionState.connected:
            print("connected")
            delegate?.didChangeConnectionState(state: kConnected, deviceName: peerID.displayName)
            
        case MCSessionState.connecting:
            print("connecting")
            delegate?.didChangeConnectionState(state: kStartedConnecting, deviceName: peerID.displayName)
        
        case MCSessionState.notConnected:
            print("notConnected")
            delegate?.didChangeConnectionState(state: kConnectionFailed, deviceName: peerID.displayName)
        }
        print("peer: \(peerID) didChangeState: \(state)")
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        print("didRecieveData: \(data)")
        
        let str = String(data: data, encoding: .utf8)!
        
        print("\n\n\n\n recieve Data \n\n \(str)")
        
        DispatchQueue.main.async { 
            
            self.delegate?.didRecieveData(dataStr: str)
            
//            let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
//            topWindow.rootViewController = UIViewController()
//            topWindow.windowLevel = UIWindowLevelAlert + 1
//            
//            let alertController = UIAlertController(title: nil,  message: str, preferredStyle: .alert)
//            alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: {
//                _ in
//                topWindow.isHidden = true
//            }))
//            
//            topWindow.makeKeyAndVisible()
//            topWindow.rootViewController?.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        print("didRecieveStream")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        print("didStartRecievingResourcesWithName")
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL, withError error: Error?) {
        print("didFinishRecievingResourcesWithName")
    }
}

//extension DataServiceManager : MCNearbyServiceAdvertiserDelegate {
//    
//    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
//        print("didNotstartAdvertisingPeer: \(error)")
//    }
//    
//    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
//        print("didRecieveInvitationFromPeer: \(peerID)")
//        invitationHandler(true, self.session)
//    }
//    
//}

extension DataServiceManager : MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("didNotStartBrowsingForPeers  : \(error)")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        print("foundPeer: \(peerID)")
        print("invitePeer: \(peerID)")
        delegate?.didChangeConnectionState(state: kStartedConnecting, deviceName: peerID.displayName)
        browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 86400)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("lostPeer : \(peerID)")
        delegate?.didChangeConnectionState(state: kDisconnected, deviceName: peerID.displayName)
    }
}
