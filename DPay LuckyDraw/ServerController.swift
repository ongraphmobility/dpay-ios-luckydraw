//
//  ServerController.swift
//  Qido
//
//  Created by Ongraph on 18/09/17.
//  Copyright © 2017 Ongraph. All rights reserved.
//

import UIKit

@objc protocol ServerControllerDelegate: class {
    func requestFinished(dictionary : [String:AnyObject])
    // @objc optional func invalidToken()
}

public enum HTTPMethod: String {
    
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
    
}

class ServerController: NSObject {
    
    weak var delegate : ServerControllerDelegate?
    
    var count = 0
    
    func CallService(urlString: String, parameters: AnyObject, method: HTTPMethod){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        print("Requesting....")
        print("Request Count - \(self.count)")
        print(urlString)
        print(parameters as Any)
        print( method.rawValue)
        
        //method for GET requests
        var urlStr = urlString
        if method == .get, let param = parameters as? String{
            urlStr += "?\(param)"
        }
        
        let url = URL(string: urlStr)
        var request = URLRequest(url: url!)
        
        request.httpMethod = method.rawValue
        
        if let param = parameters as? String, method != .get{
            request.httpBody = param.data(using: String.Encoding.utf8)
        }
        
        
        if let param = parameters as? NSDictionary, param.count > 0{
            
            do{
                let jsonData = try JSONSerialization.data(withJSONObject: param)
                request.httpBody = jsonData
                
            } catch {
                SVProgressHUD.dismiss()
                print(error.localizedDescription)
                return
                
            }
        }
        
//        if let authToken = UserDefaults.standard.getAccessKey(){
//            let dict = [kParamAuthTokenHeader : authToken]
//            request.allHTTPHeaderFields = dict
//        }
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request)
        {(data, response, error) in
            
            guard let data = data, error == nil else{
                print((error?.localizedDescription)!)
                
                DispatchQueue.main.async(execute: {
                    
                    if self.count < 3{
                        self.count += 1
                        self.CallService(urlString: urlString, parameters: parameters, method: method)
                    }else{
                        self.count = 0
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if let errorMsg = error?.localizedDescription{
                            SVProgressHUD.dismiss()
                            global.alert(title: nil, message: errorMsg, cancelButtonTitle: kOK)
                        }
                    }
                    
                })
                return
            }
            
            //            if let httpResponse = response as? HTTPURLResponse {
            //                print("httpResponse: \(httpResponse)")
            //                print("statusCode: \(httpResponse.statusCode)")
            //                if httpResponse.statusCode == 401{
            //                    DispatchQueue.main.async(execute: {
            //                        SwiftSpinner.hide()
            //                        self.delegate?.invalidToken!()
            //                    })
            //
            //                    return
            //                }
            //            }
            
            
            let responseString = String(data: data, encoding: .utf8)
            
            print(responseString!)
            
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                
                DispatchQueue.main.async(execute: {
                    
                    self.count = 0
                    self.delegate?.requestFinished(dictionary: json as! [String : AnyObject])
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                })
                
                print("request completed!!!")
                
                //  if let message = (json as! [String : AnyObject])["message"] as? String{
                //  }
                
            }catch let jsonError{
                print(jsonError)
                
                print(response as Any)
                
                DispatchQueue.main.async(execute: {
                    
                    if self.count < 3{
                        self.count += 1
                        self.CallService(urlString: urlString, parameters: parameters, method: method)
                    }else{
                        self.count = 0
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        SVProgressHUD.dismiss()
                        global.alert(title: nil, message: kAlertSomethingWentWrong, cancelButtonTitle: kOK)
                    }
                })
            }
        }
        task.resume()
    }
}
