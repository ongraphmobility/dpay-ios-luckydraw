//
//  Constants.swift
//  Qido
//
//  Created by Ongraph on 18/09/17.
//  Copyright © 2017 Ongraph. All rights reserved.
//

import Foundation
import UIKit


// MARK: - StoryBoard Names

let kStoryboardMainName = "Main"

// MARK: - StoryBoards

let kStoryboardMain = UIStoryboard(name: kStoryboardMainName, bundle: nil)


// MARK: - Constants

let kStartedConnecting = "connecting"
let kSearching = "Searching"
let kConnected = "connected"
let kDisconnected = "disConnected"
let kConnectionFailed = "failed"
let kNoDeviceAvailable = "noDevice"

let infoStartedConnecting = "Connecting with "
let infoConnected = "Connected with "
let infoSearching = "Searching for available device"
let infoDisconnected = "Disconnected with "
let infoFailed = "Unable to Connect with "
let infoRetry = "Something went wrong. Retry!"
let infoUnAvailable = "No Device Available!!!"

//let kPasswordCharacterCount = 3
let kOK = "OK"
let kRetry = "Retry"
//let kYES = "YES"
//let kNO = "NO"
//let kCancel = "CANCEL"
//let kCell = "Cell"
//let kDeviceType = "iOS"
//let kMemberId = "Member ID"
//let KLOCALE = "en_US_POSIX"
//let KDATEFORMAT1 = "dd-MM-yyyy HH:mm"
//
let kLoading = "Loading..."
//
let kHTTP200 = 200
//let kHTTP400 = 400
//let kHTTP401 = 401
//let kHTTP404 = 404

// MARK: - URLs

let kBaseUrl = "http://52.77.62.148/rest/"
let kUrlValidateUser = "\(kBaseUrl)dkg/luckyDraw"

// MARK: - Param Constants
let kParamMemberId = "memberId"
let kParamHttpStatus = "httpStatus"
let kParamData = "data"

// MARK: - Alerts

//let kAlertPasswordCount = "Password must contain more than \(kPasswordCharacterCount) characters"
let kAlertSomethingWentWrong = "Something went wrong. Please try again later!"
let kAlertInvalidNumber = "Invalid Number Entered"
let kAlertInvalidEmail = "Please enter valid email address."
let kAlertInvalidMember = "Not a DKG Member"
//let kAlertBlankUsername = "Please enter username."
let kAlertBlankFields = "All Fields are mandatory."
//let kAlertPasswordNotMatched = "Passwords not matching. Please check the passwords."
//let kAlertInvalidCredentials = "Incorrect credentials"
let kAlertLostConnection = "Lost connection with "


// MARK: - Functions


