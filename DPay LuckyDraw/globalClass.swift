//
//  globalClass.swift
//  Qido
//
//  Created by Ongraph on 18/09/17.
//  Copyright © 2017 Ongraph. All rights reserved.
//

import UIKit

class globalClass: NSObject {
    
    //  MARK: - Base URL
    //    func getBaseUrl() -> String {
    //        let url:String = "http://13.228.44.209/rest/"
    //        return url
    //    }
    
    //    func getImagesBaseUrl() -> String {
    //        let url:String = ""
    //        return url
    //    }
    
    // MARK: - Generate Random 4 digit No in String
    func random4DigitString() -> String {
        let min: UInt32 = 0001
        let max: UInt32 = 9999
        let i = min + arc4random_uniform(max - min + 1)
        return String(i)
    }
    
    // MARK: - TextField PlaceHolder Color or Text
    func setTextFieldPlaceholder(textField: UITextField, text: String, textColor:UIColor) {
        textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: textColor])
    }
    
    // MARK: - Add Left Side Image on textfield
    func addTxtFldLeftView(textfield: UITextField, image: UIImage) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        let imgView = UIImageView(frame: CGRect(x: 12, y: 12, width: 26, height: 26))
        imgView.contentMode = .scaleAspectFit
        imgView.image = image
        paddingView.addSubview(imgView)
        textfield.leftView = paddingView
        textfield.leftViewMode = UITextFieldViewMode.always
    }
    
    // MARK: - Verify Indian Mobile
    func isNumberValidDomestic(_ number: String) -> Bool {
        let numberRegEx = "[0-9]{10}"
        let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        
        let result = numberTest.evaluate(with: number)
        
        if !result {
            alert(title: nil, message: kAlertInvalidNumber, cancelButtonTitle: kOK)
        }
        
        return result
    }
    
    // MARK: - Verify Internation Mobile
    func isNumberValidInternal(_ number: String) -> Bool {
        let numberRegEx = "^((\\+)|(00))[0-9]{6,14}$"
        let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let result = numberTest.evaluate(with: number)
        
        if !result {
            alert(title: nil, message: kAlertInvalidNumber, cancelButtonTitle: kOK)
        }
        
        return result
    }
    
    // MARK: - Email Validator
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluate(with: testStr)
        
        if !result {
            alert(title: nil, message: kAlertInvalidEmail, cancelButtonTitle: kOK)
        }
        
        return result
    }
    
    // MARK: - is any textfield empty between multiples
    func isAnyTextFieldEmpty(textFields:[UITextField]) -> Bool{
        for textField in textFields {
            if let text = textField.text, text.characters.count < 1{
                alert(title: nil, message: kAlertBlankFields, cancelButtonTitle: kOK)
                return true
            }
        }
        return false
    }
    
    // MARK: - Show Alert
    func alert(title:String?,message:String,cancelButtonTitle:String){
        if message.characters.count > 0{
            let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
            topWindow.rootViewController = UIViewController()
            topWindow.windowLevel = UIWindowLevelAlert + 1
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: cancelButtonTitle, style: .default, handler: {
                _ in
                topWindow.isHidden = true
            }))
            
            topWindow.makeKeyAndVisible()
            topWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Convert hex to UIColor
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
