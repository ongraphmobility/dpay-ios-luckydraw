//
//  ViewController.swift
//  DPay LuckyDraw
//
//  Created by Ongraph on 16/11/17.
//  Copyright © 2017 Ongraph Technologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController, recieveDataDelegate, UIWebViewDelegate, ServerControllerDelegate {
    
    var dataServiceManager = DataServiceManager()
    
    @IBOutlet weak var connectionMsgLbl: UILabel!
    
    @IBOutlet weak var retryConnecting: UIButton!
    
    @IBOutlet weak var connectionView: UIView!
    
    @IBOutlet weak var myWebVw: UIWebView!
    
    @IBOutlet weak var imagViewTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        didChangeConnectionState(state: kNoDeviceAvailable, deviceName: nil)
        dataServiceManager.delegate = self
        self.manageImageView(toShow: true)
        
    }
    
    func didChangeConnectionState(state: String, deviceName: String?) {
        switch state {
        case kStartedConnecting:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                self.retryConnecting.isHidden = true
                self.connectionMsgLbl.text = infoStartedConnecting + deviceName!
            }
        case kSearching:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
                self.retryConnecting.isHidden = true
                self.connectionMsgLbl.text = infoSearching
            }
            
        case kConnected:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 0.5333333333, alpha: 1)
                self.retryConnecting.isHidden = true
                self.connectionMsgLbl.text = infoConnected + deviceName!
            }
            
        case kDisconnected:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0.846742928, green: 0.1176741496, blue: 0, alpha: 1)
                self.retryConnecting.isHidden = false
                self.connectionMsgLbl.text = infoDisconnected + deviceName!
            }
            
        case kConnectionFailed:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0.846742928, green: 0.1176741496, blue: 0, alpha: 1)
                self.retryConnecting.isHidden = false
                self.connectionMsgLbl.text = infoFailed + deviceName!
            }
            
        case kNoDeviceAvailable:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0.846742928, green: 0.1176741496, blue: 0, alpha: 1)
                self.retryConnecting.isHidden = false
                self.connectionMsgLbl.text = infoUnAvailable
            }
            
        default:
            
            DispatchQueue.main.async {
                self.connectionView.backgroundColor = #colorLiteral(red: 0.846742928, green: 0.1176741496, blue: 0, alpha: 1)
                self.retryConnecting.isHidden = false
                self.connectionMsgLbl.text = infoRetry
            }
            
        }
    }
    
    @IBAction func retryBtnTapped(_ sender: UIButton) {
        didChangeConnectionState(state: kNoDeviceAvailable, deviceName: nil)
        self.dataServiceManager = DataServiceManager()
        dataServiceManager.delegate = self
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.show(withStatus: kLoading)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
        self.manageImageView(toShow: false)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SVProgressHUD.dismiss()
        self.showAPIfailedAlert(isApiFailed: false)
    }
    
    var MainMemberID = String()
    
    func didRecieveData(dataStr: String) {
        print(dataStr)
        self.manageImageView(toShow: true)
        self.MainMemberID = dataStr
        self.getMemberDetailFromServer(memberId: dataStr)
    }

    private func getMemberDetailFromServer(memberId : String) {
        server.delegate = self
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show(withStatus: kLoading)
        let param = [kParamMemberId:memberId]
        server.CallService(urlString: kUrlValidateUser, parameters: param as AnyObject, method: .post)
    }
    
    func requestFinished(dictionary: [String : AnyObject]) {
        print(dictionary)
        
        SVProgressHUD.dismiss()
        if let status = dictionary[kParamHttpStatus] as? Int {
            if status == kHTTP200 {
                self.LoadWebViewWithMemberId()
            } else {
                self.showAPIfailedAlert(isApiFailed: true)
            }
        }
    }
    
    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    private func LoadWebViewWithMemberId() {
        let memberId = self.MainMemberID
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let data = dateFormatter.string(from: Date())
        
        let SecretKey = "***!@#!@#&*%whosaysjengganofun!@*776as88!jj$kjs0"
        let strToConvert = data + memberId + SecretKey
        let i = MD5(string: strToConvert)
        let iStr = i.map { String(format: "%02hhx", $0) }.joined()
        
        let urlStr = "http://dkgjackpot.dkgclub.com/slot.aspx?memberid=\(memberId)&i=\(iStr)"
        
                //"http://dkgjackpot.dkgclub.com/slot.aspx?memberid=demodkg1&i=6a182fbeb57b1a445c868891e79c7436"
//        let urlStr = "https://www.google.co.in/search?hl=en&dcr=0&source=hp&ei=TZANWsKhA8yk0ASyhJqwDg&q=\(memberId)&oq=\(memberId)"
        
        if let url = URL(string: urlStr) {
            let req = URLRequest(url: url)
            self.myWebVw.loadRequest(req)
        }else if let urlwithPercentEscapes = urlStr.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed), let url = URL(string: urlwithPercentEscapes) {
            let req = URLRequest(url: url)
            self.myWebVw.loadRequest(req)
        }
    }
    
    private func showAPIfailedAlert(isApiFailed:Bool) {
        let alert = UIAlertController(title: nil, message: kAlertSomethingWentWrong, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: kRetry, style: .default, handler: { (_) in
            if isApiFailed {
                self.getMemberDetailFromServer(memberId: self.MainMemberID)
            } else {
                self.LoadWebViewWithMemberId()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func closeBtnTapped(_ sender: UIButton) {
        self.manageImageView(toShow: true)
    }
    
    private func manageImageView(toShow:Bool) {
        if toShow {
            UIView.animate(withDuration: 2.5, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.5, options: .curveEaseIn, animations: {
                self.imagViewTopConstraint.constant = 50
             //   self.view.layoutIfNeeded()
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 2.5, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                self.imagViewTopConstraint.constant = (self.view.frame.height)
                //self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

